import {Component, OnInit} from '@angular/core';
import {SocketioService} from '../socketio.service';
import {ActivatedRoute} from '@angular/router';
import {BackendService} from '../backend.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  currentUserId: string;

  messages: Array<Message> = new Array();

  message: string;

  chatPartner: number;

  loading = false;
  errorMessage;

  constructor(private route: ActivatedRoute,
              private backendService: BackendService,
              private socketioService: SocketioService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.chatPartner = params.id;
      this.messages.splice(0, this.messages.length);
      this.loading = true;
      this.errorMessage = '';
      this.currentUserId = this.backendService.getUserId();
      this.backendService.getMessages(this.chatPartner)
        .subscribe(
          (response) => {
            response.forEach((value) => {
              this.messages.push({
                sender: value['sender'],
                timestamp: value['timestamp'],
                message: value['message']
              });
            });
          },
          (error) => {
            console.error('Request failed with error');
            console.log(error);
            this.errorMessage = error;
            this.loading = false;
            return;
          },
          () => {
          }
        );
    });

    this.socketioService.socket.on('new_message', (data: string) => {
      const message: Message = JSON.parse(data);
      this.messages.push({
        sender: message.sender,
        timestamp: message.timestamp,
        message: message.message
      });
    });
  }

  sendMessage(): void {
    this.socketioService.sendMessage(this.chatPartner, this.message);
    this.message = '';
  }

}

interface Message {
  sender: string;
  timestamp: string;
  message: string;
}
