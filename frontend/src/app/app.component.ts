import {Component, OnDestroy, OnInit} from '@angular/core';
import {SocketioService} from './socketio.service';
import {BackendService} from './backend.service';
import {OpenPgpService} from "./open-pgp/open-pgp.service";
import {Router} from '@angular/router';
import {WebRTCService} from './web-rtc.service';
import {CryptoService} from './crypto.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'frontend';

  username: string;
  password: string;

  authenticated = false;

  recentChats: Array<RecentChat> = new Array<RecentChat>();

  loading = false;
  incomingcall = false;
  incomingcalldata: any;

  constructor(
    private socketioService: SocketioService,
    private backendService: BackendService,
    private openPgpService: OpenPgpService,
    private cryptoService: CryptoService,
    private router: Router) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.socketioService.disconnect();
  }

  login(): void {
    this.backendService.login(this.username, this.password)
      .subscribe(
        (response) => {
          if (response[0] !== 'OK') {
            return;
          }

          this.authenticated = true;

          this.socketioService.joinClientGroup();
          this.socketioService.on('incoming', (room: any) => {
            console.log(room);
            this.incomingcalldata = room;
            this.incomingcall = true;
            if (confirm('Accept Call')) {
              this.router.navigate(['/call/'],  { state: {room, caller: false} });
            }
          });

          this.socketioService.on('incoming_group', (meeting: any) => {
            console.log(meeting);
            this.incomingcalldata = meeting;
            this.incomingcall = true;
            if (confirm('Accept Call (Group)')) {
              this.router.navigate(['/call/'],  { state: {room: JSON.parse(meeting), caller: false, groupCall: true} });
            }
          });

          this.backendService.getRecentChats()
            .subscribe(
              (response) => {
                response.forEach((value) => {
                  this.recentChats.push({
                    id: value['id'],
                    name: value['name']
                  });
                });
              },
              (error) => {
                return;
              },
              () => {
              }
            );

          this.backendService.getKeysExist().subscribe(
          async (keysExist) => {
            if (keysExist) {
              return;
            }

            const keypair = await this.openPgpService.generateKeyPair(this.username, this.password);
            this.backendService.setKeys(keypair).subscribe();
            console.log(keypair);
          }
        );
        },
        (error) => {
          console.log('Login failed!' + error.message);
          return;
        },
        () => {
        }
      );
  }

  call(ids: any[]): void {
    this.backendService.create_call(ids).subscribe( (room) => {
      console.log(room);
      this.router.navigate(['/call'], {state: {room, caller: true, groupCall: false}});
    });
  }

  callGroup(ids: any[]): void {
    this.backendService.create_group_call(ids).subscribe( (room) => {
        console.log(room);
        this.router.navigate(['/call'], {state: {room, caller: true, groupCall: true}});
    });
  }
}

interface RecentChat {
  id: number;
  name: string;
}
