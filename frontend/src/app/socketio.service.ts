import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import Socket = SocketIOClient.Socket;
import {environment} from '../environments/environment';
import Emitter = SocketIOClient.Emitter;

@Injectable({
  providedIn: 'root'
})
export class SocketioService {

  get socket(): Socket {
    return this._socket;
  }

  private _socket: Socket;

  constructor() {   }

  joinClientGroup(): void {
    this._socket = io(environment.CHAT_SOCKET_ENDPOINT, {
      path: '/socket'
    });
  }

  emit( event: string, ...args: any[] ): Socket {
    return this.socket.emit(event, ...args);
  }

  // tslint:disable-next-line:ban-types
  public on( event: string, fn: Function ): Emitter{
    return this.socket.on(event, fn);
  }

  send( ...args: any[] ): Socket{
    return this.socket.send(...args);
  }

  disconnect(): void {
    if (this._socket != null) {
      this._socket.disconnect();
    }
  }

  sendMessage(roomId: number, message: string): void {
    this._socket.emit('new_message', JSON.stringify({
      roomId: roomId,
      message: message
    }));
  }

}
