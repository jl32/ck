import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ChatComponent} from './chat/chat.component';
import {FormsModule} from '@angular/forms';
import {SocketioService} from './socketio.service';
import { CallComponent } from './call/call.component';
import {HttpClientModule} from '@angular/common/http';
import { IncomingcallComponent } from './incomingcall/incomingcall.component';
import {OpenPgpModule} from "./open-pgp/open-pgp.module";
import { CryptoDemoComponent } from './crypto-demo/crypto-demo.component';
import { RemoteVideoComponent } from './call/remote-video/remote-video.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    CallComponent,
    IncomingcallComponent,
    CryptoDemoComponent,
    RemoteVideoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    OpenPgpModule
  ],
  providers: [SocketioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
