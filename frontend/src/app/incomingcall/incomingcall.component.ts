import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-incomingcall',
  templateUrl: './incomingcall.component.html',
  styleUrls: ['./incomingcall.component.scss']
})
export class IncomingcallComponent implements OnInit {

  @Input() item: string;
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  bla(): void {
    this.router.navigate(['/call/'],  { state: {room: this.item, caller: false} });
  }
}
