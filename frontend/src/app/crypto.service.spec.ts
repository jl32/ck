import { TestBed } from '@angular/core/testing';

import { CryptoService } from './crypto.service';

describe('CryptoService', () => {
  let service: CryptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CryptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    let webCrypto = service.deriveKey('test')
    console.log(webCrypto);
    let pgp = service.generatePGPKey();
    console.log(pgp);

  });
});
