import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CallComponent} from './call/call.component';
import {ChatComponent} from './chat/chat.component';
import {CryptoDemoComponent} from './crypto-demo/crypto-demo.component';

const routes: Routes = [
  { path: 'call', component: CallComponent },
  { path: 'crypto', component: CryptoDemoComponent },
  { path: 'chat/:id', component: ChatComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
