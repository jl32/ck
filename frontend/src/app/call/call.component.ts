import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SocketioService} from '../socketio.service';
import {WebRTCService} from '../web-rtc.service';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from '../backend.service';
import {SafeHtml} from '@angular/platform-browser';

const MEDIA_CONSTRAINTS: MediaStreamConstraints = {
  audio: true,
  video: true
};

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})
export class CallComponent implements OnInit, OnDestroy {

  authenticationToken: string;

  telephoneNumber: string;
  @ViewChild('video') video: ElementRef;
  @ViewChild('video2') video2: ElementRef;
  @ViewChild('video3') video3: ElementRef;
  @ViewChild('videoLocal') videoLocal: ElementRef;
  @ViewChild('remotes') remotes: ElementRef;
  waiting = true;
  data: SafeHtml;
  public remoteStream = [];
  public active = [];
  localSteam: MediaStream;


  constructor(private socketioService: SocketioService,
              private webRTCService: WebRTCService,
              public activatedRoute: ActivatedRoute,
              private backend: BackendService,
              private router: Router) { }

  ngOnInit(): void {
    if (!history.state || !history.state.room){
      this.router.navigate(['/'], );
      return;
    }

    this.setupWebRTC();
  }

  ngOnDestroy() {
    this.webRTCService.restRTC();
  }

  public neededForSignaling(connection, index, array){
    if (this.backend.isMe(connection.caller) || this.backend.isMe(connection.participants[0])){
      return connection;
    }
  }

  setupWebRTC(): void {

    if (history.state.groupCall){
      navigator.mediaDevices.getUserMedia((MEDIA_CONSTRAINTS)).then((stream) => {
        console.log('Got stream with constraints:', MEDIA_CONSTRAINTS);
        console.log(stream);
        this.localSteam = stream;
        this.videoLocal.nativeElement.srcObject = this.localSteam;

        let connections = history.state.room.filter((connection, index, array) => {
          if (this.backend.isMe(connection.caller.toString()) || this.backend.isMe(connection.participants[0].toString())){
            return connection;
          }});

        this.webRTCService.setupRTCMeeting(this.backend.getUserId(), connections, this.localSteam, (e, room)  => {
          console.log('onTrack');
          console.log(room);
          if (!this.active.includes(room.id)){
            this.active.push(room.id);
            this.remoteStream.push(e);
          }
        });
      }).catch(error => {
        console.log(error);
      });
    } else {
      navigator.mediaDevices.getUserMedia((MEDIA_CONSTRAINTS)).then((stream) => {
        console.log('Got stream with constraints:', MEDIA_CONSTRAINTS);
        console.log(stream);
        this.videoLocal.nativeElement.srcObject = stream;
        this.webRTCService.setupRTCMeeting(this.backend.getUserId(), [history.state.room], stream, (e, room) => {
          console.log('onTrack');
          if (this.video.nativeElement.srcObject !== e.streams[0]) {
            this.video.nativeElement.srcObject = e.streams[0];
            console.log('Received remote stream');
          }
        });
      }).catch(error => {
        console.log(error);
      });
    }






    /*this.webRTCService.onLocalStream = stream => {
      this.videoLocal.nativeElement.srcObject = stream;
    };
    this.webRTCService.onReady = (room, isCaller) => {
      console.log('exec onReady', isCaller , room);
      if (isCaller){
        this.socketioService.on('start', (data) => {
          console.log('first_client_ready received');
          if (this.waiting) {
            console.log('first_client_ready starting call');
            this.waiting = false;
            this.webRTCService.call();
          }
        });
      } else {
        console.log('send first_client_ready');
        this.socketioService.emit('start', {room: room.id});
      }
    };
    this.webRTCService.setupRTC(this.backend.getUserId(), history.state.room );
    this.webRTCService.onTrack(e => {
      console.log('onTrack');
      if (this.video.nativeElement.srcObject !== e.streams[0]) {
        this.video.nativeElement.srcObject = e.streams[0];
        console.log('Received remote stream');
      }
    });*/
  }

  call(): void {
    this.webRTCService.call();
  }

  share() {
    const mediaDevices = navigator.mediaDevices as any;
    const stream = mediaDevices.getDisplayMedia({logicalSurface: true}).then(e => console.log(e));
  }
}
