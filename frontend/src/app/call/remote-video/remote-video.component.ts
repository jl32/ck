import {Component, ElementRef, Input, AfterViewInit, ViewChild} from '@angular/core';
import {observableToBeFn} from 'rxjs/internal/testing/TestScheduler';

@Component({
  selector: 'app-remote-video',
  templateUrl: './remote-video.component.html',
  styleUrls: ['./remote-video.component.scss']
})
export class RemoteVideoComponent implements AfterViewInit {

  constructor() { }

  @ViewChild('videoElement') videoElement: ElementRef;
  @Input() video: RTCTrackEvent;


  ngAfterViewInit(): void {
    this.videoElement.nativeElement.srcObject = this.video.streams[0];
  }



}
