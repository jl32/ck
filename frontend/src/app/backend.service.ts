import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {KeyPair} from "./open-pgp/key-pair";

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  getRecentChats(): Observable<any> {
    return this.http.get('/api/recent_chats');
  }

  getMessages(roomId: number): Observable<any> {
    return this.http.get('/api/messages' + '?room_id=' + roomId);
  }

  login(user: string, pass: string): Observable<any> {
    return this.http.post('/api/login', {username: user, password: pass});
  }

  create_call(participants: any): Observable<any>{
    return this.http.post('/api/call/create', {participants});
  }

  create_group_call(participants: any[]): Observable<any>{
    return this.http.post('/api/call/group/create', {participants});
  }

  setKeys(keyPair: KeyPair): Observable<any> {
    return this.http.post('/api/keys/set', {
      private_key: keyPair.privateKey,
      public_key: keyPair.publicKey
    });
  }

  getKeysExist(): Observable<any> {
    return this.http.get('/api/keys/exists');
  }

  getUserId(): string{
    return this.getCookie('user_id');
  }

  isMe(id: string): boolean {
    if (this.getUserId().toString() === id) {
      return true;
    }
    return false;
  }

  private getCookie(name): string {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2){
      return parts.pop().split(';').shift();
    }
    return null;
  }

}
