import { Injectable } from '@angular/core';
import {SocketioService} from './socketio.service';
import {WebRTC} from './webrtc';

const ICE_SERVERS: RTCIceServer[] = [
  {urls: ['stun:stun.nextcloud.com:443']}
];

const PEER_CONNECTION_CONFIG: RTCConfiguration = {
  iceServers: ICE_SERVERS
};


export const MEDIA_CONSTRAINTS: MediaStreamConstraints = {
  audio: true,
  video: true
};

const offerOptions: RTCOfferOptions = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

interface Room {
  id: string;
  caller: string;
  participants: any[];
}

@Injectable({
  providedIn: 'root'
})
export class WebRTCService {

  private peerConnection: RTCPeerConnection;
  private rtcMeeting: WebRTC[] = [];
  private meetingParticipants: any;
  private localStream: MediaStream;
  private room: Room;
  private meeting: Room[];
  private me: string;
  public onLocalStream;
  public onReady;


  constructor(private socketioService: SocketioService) { }

  public restRTC(){
    this.endCall();
  }

  public endCall(){

  }

  public setupCall(userId: string, room: Room, media: any, onNewTrack: any){
    this.me = userId;

    this.room = room;

  }

  public setupRTC(userId: string, room: any): void {
    this.me = userId;
    console.log('room ===>', room);
    if (typeof room !== 'object'){
      this.room = JSON.parse(room);
    } else {
      this.room = room;
    }
    this.peerConnection = new RTCPeerConnection(PEER_CONNECTION_CONFIG);
    this.getMedia();
    this.peerConnection.onicecandidate = this.getIceCandidateCallback();
    this.socketioService.on('sdp', this.getSDPCallback());
    this.socketioService.on('ice', this.getICECallback());
    if (this.room.caller.toString() === this.me) {
      console.log('i am caller');
      this.onReady(this.room, true);
    }
  }

  public setupRTCMeeting(userId: string, meeting: Room[], media: MediaStream, onNewTrack: any): void{
    this.me = userId;

    this.meeting = meeting;

    this.meeting.forEach((connection) => {
      let webrtc = new WebRTC(this.socketioService, this.me, connection, media, onNewTrack);
      webrtc.setup((room, isCaller) => {
        console.log('exec onReady', isCaller , room);
        if (isCaller){
          this.socketioService.on('start', (data) => {
            console.log('peer_ready received');
            if (room.id == data.room) {
              console.log('peer_ready starting call');
              webrtc.call();
            }
          });
        } else {
          console.log('send peer_ready');
          this.socketioService.emit('start', {room: room.id});
        }
      });
      this.rtcMeeting.push(webrtc);
    });

  }

  public onTrack(callback: any): void {// TODO: replace any with something use fully
    this.peerConnection.ontrack = callback;
  }


  public call(): void{
    this.peerConnection
      .createOffer(offerOptions )
      .then(this.setDescriptionOffer())
      .catch(console.error);

  }

  public answer(): void{
    this.peerConnection.createAnswer(offerOptions)
      .then(this.setDescriptionAnswer())
      .catch(console.error);
  }


  private getMedia(): void{
    navigator.mediaDevices.getUserMedia((MEDIA_CONSTRAINTS)).then((stream) => {
      console.log('Got stream with constraints:', MEDIA_CONSTRAINTS);
      console.log(stream);
      this.localStream = stream;
      this.onLocalStream(stream);
      this.localStream.getTracks().forEach((track) => {
        this.peerConnection.addTrack(track, this.localStream);
      });
      if (this.room.caller.toString() !== this.me) {
        console.log('room - onReady ===>', this.room);
        this.onReady(this.room, false);
      }
    }).catch(error => {
      console.log(error);
    });
  }

  private getIceCandidateCallback(): (event) => void{
    return (event) => {
      if (event.candidate != null) {
        //this.ice_local.push({room: this.room.id, ice: event.candidate, uuid: this.me });
        this.socketioService.emit('ice', {room: this.room.id, ice: event.candidate, uuid: this.me });
      }
    };
  }


  private setDescriptionOffer(): (description) => void {
    return (description) => {
      console.log('send descriptionOffer');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          this.socketioService.emit('sdp', {
            room: this.room.id,
            sdp: this.peerConnection.localDescription,
            uuid: this.me });
        })
        .catch(console.log);
    };
  }

  private setDescriptionAnswer(): (description) => void { // TODO: brakes if room is not the id of the caller
    return (description) => {
      console.log('send descriptionAnswer');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          this.socketioService.emit('sdp', {
            room: this.room.id,
            sdp: this.peerConnection.localDescription,
            uuid: this.room });
        })
        .catch(console.error);
    };
  }
  // TODO: generic approach of setDescriptionAnswer and setDescriptionOffer
  private setDescription(uuid: string): (description) => void {
    return (description) => {
      console.log('got description');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          this.socketioService.emit('incoming', {
            room: this.room,
            sdp: this.peerConnection.localDescription,
            uuid });
        })
        .catch(console.log);
    };
  }

  private getICECallback(): (ice) => void {
    return (iceSignal) => {
      if (iceSignal.uuid === this.me) {
        return;
      }
      if (iceSignal.ice) {
        // console.log(iceSignal);

        this.peerConnection.addIceCandidate(new RTCIceCandidate(iceSignal.ice)).then(
          (value) => {
            console.log('set ice Ok', iceSignal);
          }
        ).catch((e) => {
          console.error('error by setting ice', e);
        });
      }
    };
  }

  private getSDPCallback(): (message) => void {
    return (message) => {
      const signal = message;

      if (signal.uuid === this.me) {
        return;
      }

      console.log('sdp from remote');
      console.log(signal);
      console.log('=====================');

      if (signal.sdp) {
        console.log('remote Signal');
        console.log(signal);
        this.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp))
           .then(() => {
             /*this.ice_local.forEach(ice => {this.peerConnection.addIceCandidate(new RTCIceCandidate(ice)).catch((e) => {
               console.error('fail ice', e);
             }); });*/
             if (signal.sdp.type === 'offer') {
               this.peerConnection.createAnswer(offerOptions)
                .then(this.setDescriptionAnswer())
                .catch(console.log);
             } else {
              console.log(signal);
              console.log('strange');
            }
           })
           .catch(console.log);
      }
    };
  }



}
