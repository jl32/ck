import {SocketioService} from './socketio.service';

const ICE_SERVERS: RTCIceServer[] = [
  {urls: ['stun:stun.nextcloud.com:443']}
];

const PEER_CONNECTION_CONFIG: RTCConfiguration = {
  iceServers: ICE_SERVERS
};


const MEDIA_CONSTRAINTS: MediaStreamConstraints = {
  audio: true,
  video: true
};

const offerOptions: RTCOfferOptions = {
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

interface Room {
  id: string;
  caller: string;
  participants: any[];
}
export class WebRTC {

  private peerConnection: RTCPeerConnection;
  private rtcMeeting: RTCPeerConnection[];
  private meetingParticipants: any;
  private localStream: MediaStream;
  private me: string;
  private iceLocal = [];
  public onLocalStream;





  constructor(private socketioService: SocketioService,
              private userId: string,
              private room: Room,
              private media: any,
              private onNewTrack: any,
              ) {
    this.me = userId;
    console.log('room ===>', room);
    if (typeof room !== 'object'){
      this.room = JSON.parse(room);
    } else {
      this.room = room;
    }
    this.localStream = media;
  }


  private onTrack(): (e) =>  void{
    return (e) => {
      this.onNewTrack(e, this.room);
    };
  }

  public async setup(onReady){
    this.peerConnection = new RTCPeerConnection(PEER_CONNECTION_CONFIG);
    this.peerConnection.ontrack = this.onTrack();
    this.peerConnection.onicecandidate = this.getIceCandidateCallback();
    this.socketioService.on('sdp', this.getSDPCallback());
    this.socketioService.on('ice', this.getICECallback());
    if (this.room.caller.toString() === this.me) {
      console.log('i am caller');
      onReady(this.room, true);
    }
    this.media.getTracks().forEach((track) => {
      this.peerConnection.addTrack(track, this.media);
    });
    if (this.room.caller.toString() !== this.me) {
      onReady(this.room, false);
    }


  }

  public restRTC(){
    this.endCall();
    this.peerConnection = null;
    this.room = null;
    this.localStream = null;
    this.iceLocal = [];
    this.onLocalStream = null;
    //this.onReady = null;
  }

  public endCall(){
    if (this.peerConnection) {
      this.peerConnection.close();
    }

  }

  public isCaller(): boolean {
    if (this.room.caller.toString() !== this.me) {
      return true;
    }
    return false;
  }


  public call(): void{
    this.peerConnection
      .createOffer(offerOptions )
      .then(this.setDescriptionOffer())
      .catch(console.error);

  }

  public answer(): void{
    this.peerConnection.createAnswer(offerOptions)
      .then(this.setDescriptionAnswer())
      .catch(console.error);
  }

  private getIceCandidateCallback(): (event) => void{
    return (event) => {
      if (event.candidate != null) {
        this.iceLocal.push({room: this.room.id, ice: event.candidate, uuid: this.me });
        this.socketioService.emit('ice', {room: this.room.id, ice: event.candidate, uuid: this.me });
      }
    };
  }


  private setDescriptionOffer(): (description) => void {
    return (description) => {
      console.log('send descriptionOffer');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          this.socketioService.emit('sdp', {
            room: this.room.id,
            sdp: this.peerConnection.localDescription,
            uuid: this.me });
        })
        .catch(console.log);
    };
  }

  private setDescriptionAnswer(): (description) => void { // TODO: brakes if room is not the id of the caller
    return (description) => {
      console.log('send descriptionAnswer');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          this.socketioService.emit('sdp', {
            room: this.room.id,
            sdp: this.peerConnection.localDescription,
            uuid: this.room });
        })
        .catch(console.error);
    };
  }
  // TODO: generic approach of setDescriptionAnswer and setDescriptionOffer
  private setDescription(uuid: string): (description) => void {
    return (description) => {
      console.log('got description');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          this.socketioService.emit('incoming', {
            room: this.room,
            sdp: this.peerConnection.localDescription,
            uuid });
        })
        .catch(console.log);
    };
  }

  private getICECallback(): (ice) => void {
    return (iceSignal) => {
      if (iceSignal.uuid === this.me) {
        return;
      }
      if (iceSignal.ice && iceSignal.room == this.room.id) {
        // console.log(iceSignal);

        this.peerConnection.addIceCandidate(new RTCIceCandidate(iceSignal.ice)).then(
          (value) => {
            console.log('set ice Ok', iceSignal);
          }
        ).catch((e) => {
          console.error('error by setting ice', e);
        });
      }
    };
  }

  private getSDPCallback(): (message) => void {
    return (message) => {
      const signal = message;

      if (signal.uuid === this.me) {
        return;
      }

      console.log('sdp from remote');
      console.log(signal);
      console.log('=====================');

      if (signal.sdp && signal.room == this.room.id) {
        console.log('remote Signal');
        console.log(signal);
        this.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp))
          .then(() => {
            this.iceLocal.forEach(ice => {this.peerConnection.addIceCandidate(new RTCIceCandidate(ice)).catch((e) => {
              console.error('fail ice', e);
            }); });
            if (signal.sdp.type === 'offer') {
              this.peerConnection.createAnswer(offerOptions)
                .then(this.setDescriptionAnswer())
                .catch(console.log);
            } else {
              console.log(signal);
              console.log('strange');
            }
          })
          .catch(console.log);
      }
    };
  }



}
