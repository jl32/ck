export class KeyPair {

  constructor(
    public readonly privateKey: string,
    public readonly publicKey: string,
    public readonly passphrase: string
  ) {}
}
