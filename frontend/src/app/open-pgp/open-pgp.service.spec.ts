import { TestBed } from '@angular/core/testing';

import { OpenPgpService } from './open-pgp.service';

describe('OpenPgpService', () => {
  let service: OpenPgpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenPgpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
