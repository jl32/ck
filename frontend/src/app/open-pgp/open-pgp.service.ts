import 'openpgp';
import {Injectable} from "@angular/core";
import {KeyPair} from "./key-pair";

export const OPENPGP = require('openpgp');

@Injectable()
export class OpenPgpService {

  private openpgp;

  constructor() {
    this.openpgp = OPENPGP;
  }

  public async generateKeyPair(username: string, passphrase: string): Promise<KeyPair> {
    const {privateKeyArmored, publicKeyArmored} = await this.openpgp.generateKey({
      userIds: [{name: username}],
      curve: 'ed25519',
      passphrase: passphrase
    });

    return new KeyPair(privateKeyArmored, publicKeyArmored, passphrase);
  }

  public async encrypt(data: string, keyPair: KeyPair, recipientPublicKey: string): Promise<string> {
    const message = this.openpgp.message.fromText(data);
    const {keys: [publicKey]} = await this.openpgp.key.readArmored(recipientPublicKey);
    const {keys: [privateKey]} = await this.openpgp.key.readArmored(keyPair.privateKey);

    await privateKey.decrypt(keyPair.passphrase);

    const {data: encrypted} = await this.openpgp.encrypt({
      message: message,
      publicKeys: [publicKey],
      privateKeys: [privateKey]
    });

    return encrypted;
  }

  public async decrypt(data: string, keyPair: KeyPair, senderPublicKey: string): Promise<string> {
    const encryptedMessage = await this.openpgp.message.readArmored(data);
    const {keys: [publicKey]} = await this.openpgp.key.readArmored(senderPublicKey);
    const {keys: [privateKey]} = await this.openpgp.key.readArmored(keyPair.privateKey);

    await privateKey.decrypt(keyPair.passphrase);

    const {data: encrypted} = await this.openpgp.decrypt({
      message: encryptedMessage,
      publicKeys: [publicKey],
      privateKeys: [privateKey]
    });

    return encrypted;
  }

}
