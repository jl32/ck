import {NgModule} from '@angular/core';
import {OpenPgpService} from "./open-pgp.service";


@NgModule({
  providers: [
    OpenPgpService
  ]
})
export class OpenPgpModule {}
