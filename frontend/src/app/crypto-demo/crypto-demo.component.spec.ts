import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CryptoDemoComponent } from './crypto-demo.component';

describe('CryptoDemoComponent', () => {
  let component: CryptoDemoComponent;
  let fixture: ComponentFixture<CryptoDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CryptoDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
