import { Component, OnInit } from '@angular/core';
import {OpenPgpService} from '../open-pgp/open-pgp.service';
import {CryptoService} from '../crypto.service';

@Component({
  selector: 'app-crypto-demo',
  templateUrl: './crypto-demo.component.html',
  styleUrls: ['./crypto-demo.component.scss']
})
export class CryptoDemoComponent implements OnInit {
  public: any;
  private: any;
  data: any;
  password = '';
  result = '';

  constructor(private openPgpService: OpenPgpService,
              private cryptoService: CryptoService) { }

  ngOnInit(): void {
  }

  public genPGP(){
    this.openPgpService.generateKeyPair('test', 'test').then((keypair) => {
      this.public = keypair.publicKey;
      this.private = keypair.privateKey;
    });
  }

  public deriveKey(){
    this.cryptoService.deriveKey(this.password).then((key) => {
      console.log('deriveKey', key);
    });
  }

  public encryptWebAPI(){
    this.cryptoService.encrypt(this.data, 'iv123456789test').then((encrypted) => {
      console.log(encrypted);
      console.log(new Uint8Array(encrypted));
      this.result = this.cryptoService.bufferToBase64(new Uint8Array(encrypted));
    });
  }

  public decryptWebAPI(){
    this.cryptoService.decrypt(this.cryptoService.base64ToBuffer(this.data), 'iv123456789test').then((encrypted) => {
      this.result = this.cryptoService.dec.decode(encrypted);
    });
  }

}
