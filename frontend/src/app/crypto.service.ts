import { Injectable } from '@angular/core';
import openpgp from 'openpgp';

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor() { }

  public enc = new TextEncoder();
  public dec = new TextDecoder();

  public ivLen = 16;

  public bufferToBase64(buf) {
    let binstr = Array.prototype.map.call(buf,  (ch) => {
      return String.fromCharCode(ch);
    }).join('');
    return btoa(binstr);
  }

  public base64ToBuffer(base64) {
    let binstr = atob(base64);
    let buf = new Uint8Array(binstr.length);
    Array.prototype.forEach.call(binstr,  (ch, i) => {
      buf[i] = ch.charCodeAt(0);
    });
    return buf;
  }

  private aesKey: CryptoKey;

  private getPasswordKey(password: string): Promise<CryptoKey> {
    return window.crypto.subtle.importKey(
        'raw',
        this.enc.encode(password),
        'PBKDF2',
        false,
        ['deriveKey']
      );
  }

  private  deriveKeyInternal(passwordKey, salt, keyUsage): Promise<CryptoKey> {
    return window.crypto.subtle.deriveKey(
      {
        name: 'PBKDF2',
        salt,
        iterations: 250000,
        hash: 'SHA-256',
      },
      passwordKey,
      { name: 'AES-GCM', length: 256 },
      false,
      keyUsage
    );
  }


  public async deriveKey(password: string): Promise<CryptoKey> {
    const salt = Uint8Array.from([1, 2, 3, 4, 5, 6, 4, 7, 5, 6]);
    const passwordKey = this.getPasswordKey(password).then(key => {
      console.log('deriveKey', key);
      this.deriveKeyInternal(key, salt, ['encrypt', 'decrypt']).then( ase => {
        this.aesKey = ase;
        }
      );
    });
    //this.aesKey = await this.deriveKeyInternal(passwordKey, salt, ['encrypt', 'decrypt']);
    return this.aesKey;
  }

  public encrypt(PrivatePGPKey: string, iv: string): Promise<ArrayBuffer>{
    console.log('enc', this.aesKey);
    return window.crypto.subtle.encrypt(
      {
        name: "AES-GCM",
        iv: this.enc.encode(iv),
        tagLength: 128
      },
      this.aesKey,
      this.enc.encode(PrivatePGPKey)
    );
  }

  public decrypt(encryptedPrivatePGPKey: ArrayBuffer, iv: string): Promise<ArrayBuffer>{
    console.log('decrypt', this.aesKey);
    return window.crypto.subtle.decrypt(
      {
        name: "AES-GCM",
        iv: this.enc.encode(iv),
        tagLength: 128
      },
      this.aesKey,
      encryptedPrivatePGPKey
    );
  }


  public generatePGPKey(): any {
    return openpgp.generateKey(({
      userIds: [{ name: 'Jon Smith', email: 'jon@example.com' }],
      rsaBits: 4096,
    })).then(key => {
      console.log(key);
    });

  }

  public generatePGPKeyWithPassword(password: string): any {
    return openpgp.generateKey(({
      userIds: [{ name: 'Jon Smith', email: 'jon@example.com' }],
      rsaBits: 4096,
      passphrase: password
    }));

  }

}
