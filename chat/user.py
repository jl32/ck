class User:

    def __init__(self, id, username, firstname, lastname):
        self.id = id
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.private_key = ''
        self.public_key = ''

    def dump(self):
        return {'username': self.username,
                'firstname': self.firstname,
                'lastname': self.lastname}
