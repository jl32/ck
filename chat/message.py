class Message:

    def __init__(self, id, room_id, sender, timestamp, message):
        self.id = id
        self.room_id = room_id
        self.sender = sender
        self.timestamp = timestamp
        self.message = message

    def dump(self):
        return {'room_id': self.room_id,
                'sender': self.sender,
                'timestamp': self.timestamp,
                'message': self.message}
