from chat_room import ChatRoom
from message import Message
from user import User


def user_database_result_to_user(query_result):
    if not str(query_result) == '()':
        if not len(query_result) > 1:
            first_database_result = query_result[0]
            user = User(first_database_result[1],
                        first_database_result[2],
                        first_database_result[3],
                        first_database_result[4])
            return user


def chat_rooms_result_to_chat_rooms(query_results):
    if not str(query_results) == '()':
        chat_rooms = []
        for query_result in query_results:
            chat_room = ChatRoom(query_result[0], query_result[1])
            chat_rooms = chat_rooms + [chat_room]
        return chat_rooms


def check_user_credentials_and_get_user(database, username, password):
    query_result = database.execute_query(
        "select * from ck.user where username = '" + username + "' and password = '" + password + "'")
    return user_database_result_to_user(query_result)


def get_user_by_user_id(database, user_id):
    query_result = database.execute_query("select * from ck.user where frontend_id = '" + user_id + "'")
    return user_database_result_to_user(query_result)


def get_chat_rooms_from_chat_member(database, chat_member_id):
    query_results = database.execute_query("SELECT cr.* FROM ck.chat_member cm INNER JOIN ck.chat_room cr ON cr.id = cm.room_id WHERE cm.user_frontend_id = '" + chat_member_id + "'")
    return chat_rooms_result_to_chat_rooms(query_results)


def get_messages_by_room_id(database, room_id):
    query_result = database.execute_query("select * from ck.chat_message where room_id = " + str(room_id))
    if not str(query_result) == '()':
        messages = []
        for result in query_result:
            message = Message(result[0], result[1], result[2], result[3], result[4])
            if message.room_id == room_id:
                messages = messages + [message]
        if not len(messages) == 0:
            return messages


def assert_that_user_is_in_chat_room(database, room_id, user_id):
    query_result = database.execute_query("select * from ck.chat_member where room_id = " + str(room_id) + " and user_frontend_id = '" + user_id + "'")
    if not str(query_result) == '()':
        return True


def save_message(database, room_id, sender, timestamp, message):
    table_update = database.execute_update("INSERT INTO ck.chat_message (room_id, sender_id, timestamp, message) VALUES ("
                                           + room_id + ",'"
                                           + sender + "','"
                                           + timestamp + "','"
                                           + message + "')")
    return table_update
