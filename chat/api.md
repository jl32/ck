#/api/login [post]
* request json Fields
    * username (string)
    * password (string)
* response json Fields
    * auth ok
        * cookie: authToken httpOnly
        * http code: `200`
    * auth fail
        * MIME: `text/plain`
        * http code: `401`
        
#/api/recent_chats [get]
* response json Fields
    * returns chat_rooms as json
        * id (int)
        * name (string)
    * user not in chat_room
        * http code: `401`

#/api/messages [get]
* request arguments
    * room_id
* response json Fields
    * returns messages as json
        * room_id (int)
        * sender (string contains uuid)
        * timestamp (datetime)
        * message (string)
    * user not in chat_room
        * http code: `401`

#/api/call/create [post]
* request json Fields
    * participants (Array of string (user id))
* response json Fields
    * returns an room if at least on participant online
        * id (string)
        * participants (array of strings (online participants))
        * caller (string (is your id))
        * http code: `200`
        * MIME: `text/json`
    * np participant online
        * http code: `425`

triggers an `incoming` call event by the online participants

## room json
`
{
    'id': 'room id',
    'participants': [userID]
    'caller': userID
}
`

### sdp event
`
{
    'room': roomObject,
    'sdp': BrowsersSDPObject,
    'uuid': userID
}
`

### ice event
`
{
    'room': roomObject,
    'ice': BrowsersICEObject,
    'uuid': userID
}
`

# Drafts

#/api/session/valid [get]
* request
    * cookie: authToken httpOnly ()
* response json Fields
    * session ok
        * `{'status': 'vaild session'}`
        * http code: `200`
    * session no session or invalid
        * `{'status': 'no session'}` or `{'status': 'invalid session'}`
        * http code: `401`

#/api/session/renew [get]
* request
    * cookie: authToken httpOnly
* response
    * session renew ok
        * `{'status': 'session renewed'}`
        * cookie: authToken httpOnly
        * http code: `200`
    * session renew fail
        * `{'status': 'no session'}` or `{'status': 'invalid session'}`
        * http code: `401`

#/api/crypto/pgp/privateKey [get]
* request
    * cookie: authToken httpOnly
* response
    * session renew ok
        * `[{'key': '<encrypted key>', 'internal': true, ?'passphrase': true ?}]`
        * http code: `200`
    * session renew fail
        * `{'status': 'no session'}` or `{'status': 'invalid session'}`
        * http code: `401`

#/api/crypto/pgp/privateKey [post]
_**this action an only be performed once!**_
* request
    * cookie: authToken httpOnly
    * cookie: user_id
    * request json Fields
        * key (encrypted private key)
        * salt (needs further discussion)
        * password (user password)
* response
    * upload ok
        * `{'status': 'ok''}`
        * http code: `200`
    * upload failed
        * `{'status': 'auth failed'}`
        * http code: `401`

#/api/crypto/pgp/publicKey [get]
* request parameter (chat and user can't be combined)
    * chat (the chat id)
    * user (the user id)
* response
    * chat found
        * `[{'user': '<user id>', 'keys': [<key>]}]`
        * http code: `200`
    * chat found
        * `[<key>]`
        * http code: `200`
    * chat or user not found
        * `{'status': 'not found'}`
        * http code: `404`

## Meeting/GroupCall json
`
{
    'id': 'Meeting/GroupCall id',
    'participants': ['userID']
    'caller': 'userID'
}
`

# Change

## room json rename to connection or call?
`
{
    'id': 'room id',
    'participants': 'userID'
    'caller': 'userID'
}
`