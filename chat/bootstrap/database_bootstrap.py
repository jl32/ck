
def setup(cursor):
    print('Creating schema ck...')
    cursor.execute("CREATE SCHEMA IF NOT EXISTS ck")

    print('Creating Table user...')
    cursor.execute("""CREATE TABLE IF NOT EXISTS ck.user (
      id SERIAL UNIQUE NOT NULL,
      frontend_id TEXT UNIQUE NOT NULL,
      username TEXT UNIQUE NOT NULL,
      firstname TEXT UNIQUE NOT NULL,
      lastname TEXT UNIQUE NOT NULL,
      password TEXT UNIQUE NOT NULL,
      PRIMARY KEY (id)
    )""")

    print('Creating Table chat_room...')
    cursor.execute("""CREATE TABLE IF NOT EXISTS ck.chat_room (
        id SERIAL UNIQUE NOT NULL,
        name TEXT NOT NULL,
        PRIMARY KEY (id)
    )""")

    print('Creating Table chat_message...')
    cursor.execute("""CREATE TABLE IF NOT EXISTS ck.chat_message (
        id SERIAL UNIQUE NOT NULL,
        room_id INT NOT NULL,
        sender_id TEXT NOT NULL,
        timestamp TIMESTAMP NOT NULL,
        message TEXT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (room_id) references ck.chat_room(id),
        FOREIGN KEY (sender_id) references ck.user(frontend_id)
    )""")

    print('Creating Table chat_member...')
    cursor.execute("""CREATE TABLE IF NOT EXISTS ck.chat_member (
        id SERIAL UNIQUE NOT NULL,
        room_id INT NOT NULL,
        user_frontend_id TEXT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (room_id) references ck.chat_room(id),
        FOREIGN KEY (user_frontend_id) references ck.user(frontend_id)
    )""")
    print('Committing changes...')
    cursor.execute("COMMIT")


# TODO: Implement registration and remove this
def demo_data(cursor):
    print('Inserting member mpaulsen...')
    cursor.execute("""INSERT INTO ck.user (frontend_id, username, firstname, lastname, password) VALUES (
        'f517df43-2e03-4735-a7a4-7203d687f5d4',
        'mpaulsen',
        'Manfred',
        'Paulsen',
        'd404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db'
    ) ON CONFLICT DO NOTHING""")

    print('Inserting member lgregor...')
    cursor.execute("""INSERT INTO ck.user (frontend_id, username, firstname, lastname, password) VALUES (
        '6bafbbfc-caff-4cde-bcf1-0f12593dec58',
        'lgregor',
        'Lisa',
        'Gregor',
        'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413'
    ) ON CONFLICT DO NOTHING""")

    print('Inserting member ttest...')
    cursor.execute("""INSERT INTO ck.user (frontend_id, username, firstname, lastname, password) VALUES (
        'ffeda6ac-7ca5-40c9-8cc5-80313d7a437e',
        'ttest',
        'test',
        'test',
        'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'
    ) ON CONFLICT DO NOTHING""")

    print('Inserting chat_room...')
    cursor.execute("""INSERT INTO ck.chat_room (name) VALUES (
        'Gruppenchat ueber Waschmaschinen'
    ) ON CONFLICT DO NOTHING""")

    print('Inserting chat_room...')
    cursor.execute("""INSERT INTO ck.chat_room (name) VALUES (
        'Chat with mpaulsen'
    ) ON CONFLICT DO NOTHING""")

    print('Inserting chat_member...')
    cursor.execute("""INSERT INTO ck.chat_member (room_id, user_frontend_id) VALUES (
        1,
        'f517df43-2e03-4735-a7a4-7203d687f5d4'
    ) ON CONFLICT DO NOTHING""")

    print('Inserting chat_member...')
    cursor.execute("""INSERT INTO ck.chat_member (room_id, user_frontend_id) VALUES (
        1,
        '6bafbbfc-caff-4cde-bcf1-0f12593dec58'
    )""")

    print('Inserting chat_member...')
    cursor.execute("""INSERT INTO ck.chat_member (room_id, user_frontend_id) VALUES (
        1,
        'ffeda6ac-7ca5-40c9-8cc5-80313d7a437e'
    )""")

    print('Inserting chat_message...')
    cursor.execute("""INSERT INTO ck.chat_message (room_id, sender_id, timestamp, message) VALUES (
        1,
        'f517df43-2e03-4735-a7a4-7203d687f5d4',
        '2020-11-29 14:23:06',
        'Moin!'
    )""")

    print('Inserting chat_message...')
    cursor.execute("""INSERT INTO ck.chat_message (room_id, sender_id, timestamp, message) VALUES (
        1,
        'f517df43-2e03-4735-a7a4-7203d687f5d4',
        '2020-11-29 15:44:23',
        'Warum antwortest du nicht?'
    )""")

    print('Inserting chat_message...')
    cursor.execute("""INSERT INTO ck.chat_message (room_id, sender_id, timestamp, message) VALUES (
        1,
        '6bafbbfc-caff-4cde-bcf1-0f12593dec58',
        '2020-11-29 15:47:53',
        'Ich komm ja schon.'
    )""")

    print('Committing changes...')
    cursor.execute("COMMIT")
