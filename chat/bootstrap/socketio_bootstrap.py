from logging import log, WARN
from os import environ

import redis
from flask_socketio import SocketIO

from database_connector import DatabaseConnector
from .envvar_not_found import EnvvarNotFoundException


class SocketIOBootstrap:
    app = None
    socketio = None
    redis_db = None
    postgres_db = None


    def __init__(self, app):
        self.app = app
        self.socketio = SocketIO(self.app, logger=True)


    def load_from_env(self, env_var):
        if env_var not in environ.keys():
            raise EnvvarNotFoundException('Environment variable "' + env_var + '" couldn\'t be found!')

        return environ[env_var]


    def load_from_env_or_default(self, env_var, default):
        try:
            return self.load_from_env(env_var)
        except EnvvarNotFoundException:
            return default


    def connect_redis(self):
        return redis.Redis(host=self.load_from_env_or_default('REDIS_DB_HOST', 'chat_redis'), port=6379, db=0)


    def connect_postgres(self):
        return DatabaseConnector(host=self.load_from_env_or_default('POSTGRES_DB_HOST', 'chat_postgres'),
                                 port=5432,
                                 database=self.load_from_env_or_default('POSTGRES_DB_DATABASE', 'chat_postgres'),
                                 user=self.load_from_env('POSTGRES_DB_USER'),
                                 password=self.load_from_env('POSTGRES_DB_PASSWORD'))


    def connect_call_db(self):
        return redis.Redis(host=self.load_from_env_or_default('REDIS_DB_HOST', 'chat_redis'), port=6379, db=1)


    def run_socketio(self):
        ssl_enabled = self.load_from_env_or_default('SSL_ENABLED', 'no')

        if ssl_enabled == 'no':
            log(WARN, "SSL is disabled. This is not recommended for a production instance!")
            self.socketio.run(self.app, host='0.0.0.0', port=5000, log_output=True)

            return
        elif ssl_enabled != 'yes':
            raise Exception('SSL_ENABLED should be either "yes" or "no" !')

        certfile = self.load_from_env("SSL_CERTFILE")
        keyfile = self.load_from_env("SSL_KEYFILE")

        self.socketio.run(self.app, host='0.0.0.0', port=443, log_output=True, certfile=certfile, keyfile=keyfile)
