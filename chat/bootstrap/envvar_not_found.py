class EnvvarNotFoundException(Exception):

    def __init__(self, message="A environment variables is needed but couldn't be found"):
        super().__init__(message)
