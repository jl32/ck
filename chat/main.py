import json
import random
import string
from datetime import datetime
from logging import basicConfig, INFO

from flask import Flask, request, jsonify, make_response
from flask_socketio import join_room, emit

from password_functions import hash_password
from bootstrap import SocketIOBootstrap
from bootstrap import database_bootstrap
from token_database import get_user_by_auth_token
from user_database import check_user_credentials_and_get_user, get_messages_by_room_id, save_message, \
    assert_that_user_is_in_chat_room, get_chat_rooms_from_chat_member

basicConfig(level=INFO, format='[%(asctime)s][%(levelname)s] - %(funcName)s: %(message)s')

app = Flask(__name__)
app.config['SECRET_KEY'] = 's3cr3t!'
socketio_bootstrap = SocketIOBootstrap(app)
socketio = socketio_bootstrap.socketio

redis_db = socketio_bootstrap.redis_db
postgres_db = socketio_bootstrap.postgres_db
call_db = None


@socketio.on('connect')
def connect():
    user = get_user_by_auth_token(redis_db, postgres_db)
    if user:
        join_room(user.id)
    print('Client connected')


@socketio.on('disconnect')
def disconnect():
    print('Client disconnected')


@socketio.on('incoming')
def handle_incoming(data):
    join_room(data['room'])
    print(data)
    emit('incoming', data)
    emit('new_call', data)
    try:
        emit('incoming', data, room=data['room'])
        emit('new_call', data, room=data['room'])
    except Exception as e:
        print('=====')
        print(data)
        print(e)
        print('====')


@socketio.on('ice')
def handle_message_ice(data):
    room = load_call(data['room'])
    send_to = room['participants']
    send_to.append(room['caller'])
    for p in send_to:
        if not get_user_by_auth_token(redis_db, postgres_db).id == p:
            socketio.emit('ice', data, room=str(p))
            print("send ice to " + str(p))


# TODO: check if client is caller and participants
@socketio.on('sdp')
def handle_message_sdp(data):
    room = load_call(data['room'])
    if data['sdp']['type'] == 'offer':
        for p in room['participants']:
            socketio.emit('sdp', data, room=str(p))
            print("send sdp to " + str(p))
    else:
        socketio.emit('sdp', data, room=str(room['caller']))
        print("send sdp to " + str(room['caller']))


# TODO: check if client is caller and participants
@socketio.on('start')
def handle_first_client_ready(data):
    room = load_call(data['room'])
    socketio.emit('start', data, room=str(room['caller']))
    print('emitted ready')


@app.route('/api/call/create', methods=['POST'])
def create_call():
    r = request
    caller = get_user_by_auth_token(redis_db, postgres_db)
    if not caller:
        return '{"status":"unauthorized"}', 401, {'Content-Type': 'text/json'}
    participants = json.loads(r.data)['participants']
    room = create_room_info(caller=caller.id, participants=participants)
    save_call(room)
    for participant in participants:
        # TODO: check if online
        if True:
            socketio.emit('incoming', json.dumps(room), room=str(participant))
    return room, 200, {'Content-Type': 'text/json'}


# TODO: check that all meeting members are unique
@app.route('/api/call/group/create', methods=['POST'])
def create_group_call():
    r = request
    caller = get_user_by_auth_token(redis_db, postgres_db)
    if not caller:
        return '{"status":"unauthorized"}', 401, {'Content-Type': 'text/json'}
    group = json.loads(r.data)
    meeting = []
    # TODO: make this more efficient by using index
    for participant in group['participants']:
        create = False
        # TODO: check if participant is online
        if True:
            new_caller_connection = create_room_info(caller.id, [participant])
            meeting.append(new_caller_connection)
            save_call(new_caller_connection)
            for p in group['participants']:
                if participant == p:
                    create = True
                    continue
                if not create:
                    continue
                # TODO: check if p is online
                if True:
                    new_connection = create_room_info(participant, [p])
                    meeting.append(new_connection)
                    save_call(new_connection)
    for participant in group['participants']:
        print('sending: ' + json.dumps(meeting) + 'to: ' + str(participant))
        socketio.emit('incoming_group', json.dumps(meeting), room=str(participant))
    return json.dumps(meeting), 200, {'Content-Type': 'text/json'}


def save_call(room):
    global call_db
    call_db.set(room['id'], json.dumps(room), ex=1800)


def load_call(room_id):
    global call_db
    return json.loads(call_db.get(room_id))


def create_room_info(caller, participants):
    room_info = {
        'id': ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(12)),
        'participants': participants,
        'caller': caller
    }
    return room_info


@app.route('/api/login', methods=['POST'])
def login():
    username_and_password = extract_username_and_password_from_request()
    if username_and_password:
        user = check_user_credentials_and_get_user(postgres_db, username_and_password['username'], hash_password(username_and_password['password']))
        if user:
            token = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(12))
            redis_db.set(token, user.id, ex=1800)
            response = make_response(jsonify('OK', 200, {'Content-Type': 'text/plain'}))
            response.set_cookie('authToken', value=token, httponly=True, samesite='Strict')
            response.set_cookie('user_id', value=str(user.id), samesite='Strict')
            return response
    return 'user not exists', 401, {'Content-Type': 'text/plain'}


def extract_username_and_password_from_request():
    try:
        data = json.loads(request.data)
        username = data['username']
        password = data['password']
        return {
            "username": username,
            "password": password
        }
    except KeyError:
        return None


@socketio.on('new_message')
def new_message(data):
    user = get_user_by_auth_token(redis_db, postgres_db)
    if user:
        data_json = json.loads(data)
        room_id = data_json['roomId']
        message = data_json['message']
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if save_message(postgres_db, room_id, user.id, timestamp, message):
            emit('new_message',
                 '{"sender":"' + user.id + '", "timestamp":"' + str(timestamp) + '", "message":"'
                 + str(message) + '"}', room=user.id)


@app.route('/api/recent_chats')
def recent_chats():
    user = get_user_by_auth_token(redis_db, postgres_db)
    if user:
        recent_chats = get_chat_rooms_from_chat_member(postgres_db, user.id)
        if recent_chats:
            return jsonify([recent_chat.dump() for recent_chat in recent_chats])
    return 'Unauthorized', 401, {'Content-Type': 'text/plain'}


@app.route('/api/messages')
def messages():
    room_id = int(request.args.get('room_id'))
    user = get_user_by_auth_token(redis_db, postgres_db)
    if user:
        if assert_that_user_is_in_chat_room(postgres_db, room_id, user.id):
            messages_from_database = get_messages_by_room_id(postgres_db, room_id)
            if messages_from_database:
                messages_json = [message.dump() for message in messages_from_database]
                return jsonify(messages_json)
    return 'Unauthorized', 401, {'Content-Type': 'text/plain'}


@app.route('/api/get_user_id')
def get_user_id():
    transmitter = get_user_by_auth_token(redis_db, postgres_db)
    if transmitter:
        return jsonify(transmitter.id)
    return 'Unauthorized', 401, {'Content-Type': 'text/plain'}


@app.route('/api/keys/set', methods=['POST'])
def set_keys():

    transmitter = get_user_by_auth_token(redis_db, postgres_db)

    if not transmitter:
        return 'Unauthorized', 401, {'Content-Type': 'text/plain'}

    transmitter.private_key = request.json['private_key']
    transmitter.public_key = request.json['public_key']

    print(str(transmitter))

    return jsonify('OK', 200, {'Content-Type': 'text/plain'})


@app.route('/api/keys/exists', methods=['GET'])
def keys_exist():

    transmitter = get_user_by_auth_token(redis_db, postgres_db)

    if not transmitter:
        return 'Unauthorized', 401, {'Content-Type': 'text/plain'}

    return jsonify(len(transmitter.private_key) > 0 and len(transmitter.public_key) > 0)


if __name__ == '__main__':

    postgres_db = socketio_bootstrap.connect_postgres()

    if 'true' == socketio_bootstrap.load_from_env_or_default('INIT_DB', 'false').lower():
        database_bootstrap.setup(postgres_db.conn.cursor())
    if 'true' == socketio_bootstrap.load_from_env_or_default('INIT_DB_DEMO', 'false').lower():
        database_bootstrap.demo_data(postgres_db.conn.cursor())

    redis_db = socketio_bootstrap.connect_redis()
    call_db = socketio_bootstrap.connect_call_db()
    socketio_bootstrap.run_socketio()
