import pg8000


class DatabaseConnector:

    def __init__(self, host, port, database, user, password):
        self.conn = pg8000.connect(host=host, port=int(port), database=database, user=user, password=password)
        self.cursor = self.conn.cursor()

    def disconnect(self):
        if self.cursor is not None:
            self.cursor.close()
        if self.conn is not None:
            self.conn.close()

    def execute_query(self, query_statement):
        self.cursor.execute(query_statement)
        return self.cursor.fetchall()

    def execute_update(self, update_statement):
        self.cursor.execute(update_statement)
        self.conn.commit()
        return True
