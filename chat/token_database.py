from flask import request
from werkzeug.exceptions import BadRequestKeyError

from user_database import get_user_by_user_id


def get_user_by_auth_token(redis_db, postgres_db):
    try:
        auth_token = request.cookies['authToken']
        if auth_token:
            user = get_user_by_token(redis_db, postgres_db, auth_token)
            if user:
                return user
    except BadRequestKeyError:
        return None


def get_user_by_token(redis_db, postgres_db, auth_token):
    try:
        user_id = redis_db.get(auth_token).decode('utf-8')
        return get_user_by_user_id(postgres_db, user_id)
    except AttributeError:
        return None
