server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;

    add_header  X-Robots-Tag "noindex,nofollow,nosnippet,noarchive";

    access_log /dev/stdout;
    error_log /dev/stderr info;

    return 301 https://$host$request_uri;
}

server {
    listen 443 default_server ssl;
    listen [::]:443 default_server ssl;

    server_name _;

    add_header  X-Robots-Tag "noindex,nofollow,nosnippet,noarchive";

    include conf.d/ssl_config/ssl.conf;

    access_log /dev/stdout;
    error_log /dev/stderr info;

    location / {
        root /var/www;
        try_files $uri /index.html;
    }

    location ^~ /api {
        proxy_pass https://chat_upstream;

        proxy_ssl_verify off;

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
    }

    location ^~ /socket {
        proxy_pass https://chat_upstream/socket.io;

        proxy_ssl_verify off;

        proxy_http_version 1.1;

        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
    }

    location ^~ /.well-known {
        root /var/letsencrypt_webroot/.well-known;
    }
}

map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}

upstream chat_upstream {
    ip_hash;

    server chat:443;
}