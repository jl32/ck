#!/bin/sh

if [ $# -ne 2 ]; then
  echo 'Usage: get-cert EMAIL DOMAIN'
fi

if [ -f /etc/letsencrypt/live/${1}/cert.pem ]; then
  exit 0
fi

email=$1
domain=$2
/usr/local/bin/certbot certonly -n --agree-tos --standalone -m "$email" -d "$domain"
