curl --fail -XPOST -H "Content-type: application/json" -d '{"name":"'$CI_COMMIT_REF_NAME'"}' 'https://gitlab:'$FEATURE_MANAGER_TOKEN'@'$FEATURE_MANAGER_DOMAIN'/lxd/container'
echo ""

sed -i "s#__certbot-domain__#$CI_COMMIT_REF_NAME.$FEATURE_TLD#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml
sed -i "s#__certbot-email__#$CERTBOT_EMAIL#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml
sed -i "s#__registry-url__#$CI_REGISTRY#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml
sed -i "s#__image-version-tag__#$CI_COMMIT_SHA#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml
sed -i "s#__registry-user__#$REGISTRY_USER#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml
sed -i "s#__registry-password__#$REGISTRY_TOKEN#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml
sed -i "s#__postgres_password__#$POSTGRES_PASSWORD#g" $CI_PROJECT_DIR/.ci/ansible/host_vars/localhost.yml

tar cvfz $CI_PIPELINE_ID.tar.gz .ci/ansible/

echo ""
curl --fail -F "artifact=@$CI_PIPELINE_ID.tar.gz" -F "container_name=$CI_COMMIT_REF_NAME" "https://gitlab:"$FEATURE_MANAGER_TOKEN"@"$FEATURE_MANAGER_DOMAIN"/lxd/container/upload"
echo ""